from django import forms
from .models import User,Profile
from django.contrib.admin import widgets

class UserForm(forms.ModelForm):
    rpassword = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'string optional','placeholder':'Repeat Password'}))
    class Meta:
        model = User
        fields = ['username','password','first_name','last_name','email']
        widgets = {
            'username': forms.TextInput(attrs={'class': 'string optional','placeholder':'Username'}),
            'email': forms.EmailInput(attrs={'class': 'string optional', 'placeholder': 'Email'}),
            'password': forms.PasswordInput(attrs={'class': 'string optional','placeholder':'Enter Password'}),
            'first_name': forms.TextInput(attrs={'class': 'string optional','placeholder':'Firstname'}),
            'last_name': forms.TextInput(attrs={'class': 'string optional','placeholder':'Lastname'}),
        }

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super(UserForm, self).__init__(*args, **kwargs)
    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None
class ProfileForm(forms.ModelForm):
    class Meta:
        model = Profile
        exclude=[]
        widgets = {
            'contact_address': forms.TextInput(attrs={'class': 'form-control', '': ''}),
            'country': forms.TextInput(attrs={'class': 'form-control', '': ''}),
            'telephone': forms.TextInput(attrs={'class': 'form-control', '': ''}),
            'city': forms.TextInput(attrs={'class': 'form-control'}),

        }

class UpdateForm(forms.ModelForm):
    class Meta:
        model=User
        exclude=['username','email']
        widgets = {
            'username': forms.TextInput(attrs={'class': 'form-control','disabled':''}),
            'email': forms.EmailInput(attrs={'class': 'form-control','disabled':''}),
            'first_name': forms.TextInput(attrs={'class': 'form-control','':'' }),
            'last_name': forms.TextInput(attrs={'class': 'form-control'}),

        }
        #
        # def __init__(self, *args, **kwargs):
        #     self.request = kwargs.pop('request', None)
        #     super(UpdateForm, self).__init__(*args, **kwargs)
        #
        # def get_user(self, user_id):
        #     try:
        #         return User.objects.get(pk=user_id)
        #     except User.DoesNotExist:
        #         return None

