from django.db import models
from django.contrib.auth.models import Permission, User
from datetime import datetime
from django.db.models.signals import post_save
from django.dispatch import receiver
# Create your models here.
class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    contact_address=models.TextField()
    city=models.CharField(max_length=50,blank=True)
    country=models.CharField(max_length=100,blank=True)
    company_name=models.CharField(max_length=200,blank=True)
    telephone=models.CharField(max_length=200,blank=True)

    @receiver(post_save, sender=User)
    def create_user_profile(sender, instance, created, **kwargs):
        if created:
            Profile.objects.create(user=instance)

    @receiver(post_save, sender=User)
    def save_user_profile(sender, instance, **kwargs):
        instance.profile.save()
    def __str__(self):
        return self.user.username+ " : " +self.user.first_name + self.user.last_name



class Package(models.Model):
    name=models.CharField(max_length=30)
    price=models.DecimalField(max_digits=9,decimal_places=2)
    def __str__(self):
        return self.name+":"+self.price

class Subscription(models.Model):
    user=models.ForeignKey(User,on_delete=models.CASCADE)
    package_type=models.ForeignKey(Package,on_delete=models.CASCADE)
    expiry_date=models.DateField()
    activation_date=models.DateTimeField(auto_now=True)
    def is_active(self):
        if self.expiry_date.__lt__(datetime.today().date()):
            return True
        return False

    def __str__(self):

        return self.user.username+ ":"+ self.package_type+ " - "+self.expiry_date

class Transaction(models.Model):
    description=models.CharField(max_length=250)
    user=models.ForeignKey(User,default=1)
    timestamp=models.DateTimeField(auto_now=True)

class Log(models.Model):
    description = models.CharField(max_length=250)
    timestamp=models.TextField(max_length=500)
class Notifications(models.Model):
    user=models.ForeignKey(User,on_delete=models.CASCADE,default=1)
    content=models.TextField(blank=True)
    sender=models.CharField(max_length=200)
    is_read=models.BooleanField(default=False)
    timestamp=models.DateTimeField(auto_now=True)
