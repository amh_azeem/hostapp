from django.conf.urls import url,include
from django.contrib.auth import views as auth_views
from . import views
from .form import UserForm

app_name='hosting'
urlpatterns = [
    url(r'^$',views.index,name='index'),


    url(r'^clients/login$',views.login_user,name='login_user'),
    #url(r'^clients/$', auth_views.login, {'template_name': 'hostingportal/login_form.html',
                                         # 'authentication_form':UserForm},name='login_user'),
    url(r'^clients/register$',views.register,name='register'),
    url(r'^clients/logout$',views.logout_user,name='logout_user'),
    url(r'^clients/profile$',views.profile,name='profile'),
    url(r'^clients/profile/update$',views.profile,name='profile-update'),
    url(r'^clients/notifications',views.messages,name='messages'),
    url(r'^clients/domains/', views.search_domain, name='search_domain'),
    url(r'^clients/domains/search/(?P<domain_name>[a-zA_Z]+)', views.check_domian_availability, name='search_domain'),
    url(r'^clients/notifications/(?P<message_id>[0-9]+)$',views.message_detail,name='message-detail'),
    url(r'^clients/mydomains',views.domain_list,name='domain_list'),
    url(r'^clients/validate',views.password,name='validate'),
    url(r'^clients/$',views.portal,name='portal'),
]
