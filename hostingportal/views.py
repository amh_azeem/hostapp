from django.shortcuts import render,HttpResponse,redirect,get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login,logout
from .form import UserForm,ProfileForm,UpdateForm
from django.forms.models import inlineformset_factory
from .models import User,Profile,Subscription,Notifications
from django.contrib.auth.forms import AdminPasswordChangeForm, PasswordChangeForm
from django.contrib.auth import update_session_auth_hash
from django.contrib import messages
from social_django.models import UserSocialAuth

MESSAGES_TYPE={'unread':0,'read':1}
# Create your views here.
def index(request):
    return render(request, 'hostingportal/index.html')
def validate(request):

    return render(request, 'hostingportal/login_form.html')

def login_user(request):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                return redirect('hosting:portal')
            else:
                return render(request, 'hostingportal/login_form.html', {'error_message': 'Your account has been disabled', 'form':UserForm})
        else:
            return render(request, 'hostingportal/login_form.html', {'error_message': 'Invalid login', 'form':UserForm})
    return render(request, 'hostingportal/login_form.html', {'form':UserForm})


def register(request):
    form = UserForm(request.POST or None)
    if form.is_valid():
        user = form.save(commit=False)
        username = form.cleaned_data['username']
        password = form.cleaned_data['password']
        user.set_password(password)
        user.save()
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                return render(request, 'hostingportal/clientportal.html')
    context = {"form": form}
    return render(request, 'hostingportal/login_form.html', context)




def logout_user(request):
    logout(request)
    form = UserForm(request.POST or None)
    context = {"form": form}
    return redirect('hosting:login_user')
def get_messages(request,type):
    users_messages=[]
    if type=='unread':
        users_messages = Notifications.objects.all().filter(user=request.user).filter(is_read=False)
    elif type=='read':
        users_messages = Notifications.objects.all().filter(user=request.user).filter(is_read=True)
    else:
        users_messages = Notifications.objects.all().filter(user=request.user)

    return users_messages

@login_required
def portal(request):
    messages=get_messages(request,MESSAGES_TYPE['unread'])
    return render(request, "hostingportal/clientportal.html", {'messages':messages})
@login_required
def messages(request):
    pass

@login_required
def profile(request):
    # instance = get_object_or_404(User, username=request.user.username)
    # form = UpdateForm(request.POST or None, instance=instance)
    # _user=User.objects.get(username=request.user.username)
    # ProfileInlineFormset = inlineformset_factory(User, Profile,
    #                                              fields=('contact_address', 'city', 'country'))
    # formset=ProfileInlineFormset(instance=instance)
    # context={'user':_user,'form':form,'formset':formset}
    # #return render(request, 'hostingportal/profile.html', context)
    # return render(request,'hostingportal/profile.html',context)


    # querying the User object with pk from url
    user = get_object_or_404(User, username=request.user.username)
    # prepopulate UserProfileForm with retrieved user values from above.
    user_form = UpdateForm(instance=user)
    # The sorcery begins from here, see explanation below
    _u=get_object_or_404(Profile,user=user)
    formset = ProfileForm(instance=_u)
    if request.method == "POST":
        user_form = UpdateForm(request.POST, instance=user)
        if user_form.is_valid():
            created_user = user_form.save(commit=False)
            formset = ProfileForm(request.POST, instance=created_user)
            if formset.is_valid():
                created_user.save()
                formset.save()
                context = {'error_message': 'Profile updated successfully',
                           'form': user_form
                           }

                return render(request, 'hostingportal/profile.html', context)
        else:
            return render(request, "hostingportal/profile.html", {
                "form": user_form,
                "formset": formset,
                'error_message': 'Form not valid'
            })

    return render(request, "hostingportal/profile.html", {
        "form": user_form,
        "formset": formset,
        'error_message': ''
    })

@login_required
def update_profile(request):
    # instanc=get_object_or_404(User,username=request.user.username)
    # form=UpdateForm(request.POST or None,instance=instanc)
    # if form.is_valid():
    #     form.save()
    # else:
    #     return redirect('hosting:profile')

    # querying the User object with pk from url
    user = get_object_or_404(User,username=request.user.username)

    # prepopulate UserProfileForm with retrieved user values from above.
    user_form = UserForm(instance=user)

    # The sorcery begins from here, see explanation below
    ProfileInlineFormset = inlineformset_factory(User, Profile,
                                                 fields=('contact_address', 'city', 'country'))
    formset = ProfileInlineFormset(instance=user)
    if request.method == "POST":
        user_form = UserForm(request.POST,instance=user)
        formset = ProfileInlineFormset(request.POST, instance=user)

        if user_form.is_valid():
            created_user = user_form.save(commit=False)
            formset = ProfileInlineFormset(request.POST, instance=created_user)

            if formset.is_valid():
                created_user.save()
                formset.save()
                context={'error_message':'Profile updated successfully',
                         'form':user_form
                         }

                return render(request,'hostingportal/profile.html',context)

    return render(request, "hostingportal/profile.html", {
        "form": user_form,
        "formset": formset,
        'error_message':''
    })

@login_required
def domain_list(request):
    return render(request, 'hostingportal/domain_list.html')

@login_required
def settings(request):
    user = request.user

    try:
        github_login = user.social_auth.get(provider='github')
    except UserSocialAuth.DoesNotExist:
        github_login = None

    try:
        twitter_login = user.social_auth.get(provider='twitter')
    except UserSocialAuth.DoesNotExist:
        twitter_login = None

    try:
        facebook_login = user.social_auth.get(provider='facebook')
    except UserSocialAuth.DoesNotExist:
        facebook_login = None

    can_disconnect = (user.social_auth.count() > 1 or user.has_usable_password())

    return render(request, 'core/settings.html', {
        'github_login': github_login,
        'twitter_login': twitter_login,
        'facebook_login': facebook_login,
        'can_disconnect': can_disconnect
    })

@login_required
def password(request):
    if request.user.has_usable_password():
        PasswordForm = PasswordChangeForm
    else:
        PasswordForm = AdminPasswordChangeForm

    if request.method == 'POST':
        form = PasswordForm(request.user, request.POST)
        if form.is_valid():
            form.save()
            update_session_auth_hash(request, form.user)
            messages.success(request, 'Your password was successfully updated!')
            return redirect('password')
        else:
            messages.error(request, 'Please correct the error below.')
    else:
        form = PasswordForm(request.user)
    return render(request, 'hostingportal/password.html', {'form': form})
def message_detail(request):
    pass
def search_domain(request):
    return render(request, 'hostingportal/domain_check.html')
def check_domian_availability(request,_domain):
    if _domain is not None:
        from time import sleep
        import sys
        try:
            import whois
        except ImportError:
            print("ERROR: This script requires the python-whois module to run.")
            print("   You can install it via 'pip install python-whois'")
            sys.exit(0)
        try:
            w = whois.whois(_domain)
            return render(request, 'hostingportal/domain_check.html', {'status': 'Available', 'domain_name':_domain})
        except whois.parser.PywhoisError:
            # Exception means that the domain is free
            return render(request, 'hostingportal/domain_check.html', {'status': 'Taken', 'domain_name': _domain})
    return render(request, 'hostingportal/domain_check.html')