class domain_check():

    def __init__(self):
        self.is_available=False
        self.domain_name=''
    def __init__(self,domain_list):
        self.is_available = False
        self.domain_name=domain_list


    def is_available(self):

        # Change top-level domain to check here
        while self.domain_name is not None:
            try:
                import whois
            except ImportError:
                print("ERROR: This script requires the python-whois module to run.")
            try:
                temp=whois.whois(self.domain_name)
                return False

            except whois.parser.PywhoisError:
                return True # Exception means that the domain is free


