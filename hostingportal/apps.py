from django.apps import AppConfig


class HostingportalConfig(AppConfig):
    name = 'hostingportal'
