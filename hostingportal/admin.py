from django.contrib import admin
from .models import Profile,Package,Subscription,Log,Notifications

# Register your models here.
admin.site.register(Profile)
admin.site.register(Package)
admin.site.register(Subscription)
admin.site.register(Log)
admin.site.register(Notifications)
